import numpy as np
import nltk as nlp
import random
from sklearn.linear_model import LogisticRegression
from sklearn.utils import shuffle

from bs4 import BeautifulSoup

f = open("stopwords.txt", "r")
stop_words = set()
for x in f:
  stop_words.add(x.rstrip("\n"))

positive_reviews = BeautifulSoup(open("electronics/positive.review").read(),'html5lib').find_all('review_text')
negative_reviews = BeautifulSoup(open("electronics/negative.review").read(),'html5lib').find_all('review_text')
np.random.shuffle(positive_reviews)
np.random.shuffle(negative_reviews)

def tokenize(s):
  s = s.lower()
  tokens = nlp.tokenize.word_tokenize(s)
  tokens = [ token for token in tokens if not token in stop_words and len(token)>2 ]
  return tokens

postive_tokens = list(map(lambda review: tokenize(review.text), positive_reviews))
negative_tokens = list(map(lambda review: tokenize(review.text), negative_reviews))
positive_token_list = []
negative_token_list = []

word_map_index = {}
current_index = 0
orig_reviews =[]
for tokens in postive_tokens:
  orig_reviews.append(tokens)
  for token in tokens:
    positive_token_list.append(token)
    if token not in word_map_index:
      word_map_index[token] = current_index
      current_index+=1

for tokens in negative_tokens:
  orig_reviews.append(tokens)
  for token in tokens:
    negative_token_list.append(token)
    if token not in word_map_index:
      word_map_index[token] = current_index
      current_index+=1

def tokens_to_vector(tokens,label):
  vector = np.zeros(len(word_map_index)+1)
  for token in tokens:
    index = word_map_index[token]
    vector[index]+=1
  vector = vector/vector.sum()      
  vector[-1] = label
  return vector

N = len(postive_tokens ) + len(negative_tokens)
print(N)
data = np.zeros((N, len(word_map_index) + 1))
i = 0
for tokens in postive_tokens:
    xy = tokens_to_vector(tokens, 1)
    data[i,:] = xy
    i += 1
for tokens in negative_tokens:
    xy = tokens_to_vector(tokens, 0)
    data[i,:] = xy
    i += 1

for i in range(3):
  orig_reviews, data = shuffle(orig_reviews, data)

X = data[:,:-1]
Y = data[:,-1]
print(Y)

model = LogisticRegression()
Xtrain = X[:-100,]
Ytrain = Y[:-100,]
Xtest = X[-100:,]
Ytest = Y[-100:,]
model.fit(Xtrain, Ytrain)
print("Train accuracy:", model.score(Xtrain, Ytrain))
print("Test accuracy:", model.score(Xtest, Ytest))